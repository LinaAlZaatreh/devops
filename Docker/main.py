import psycopg2


def get_user_info(id):
    conn = psycopg2.connect(
        host='172.17.0.2',
        port='5432',
        user='postgres',
        password='password',
        database='postgres'
    )
    cursor = conn.cursor()
    cursor.execute("SELECT name, email, age FROM users WHERE id=%s", (id,))
    result = cursor.fetchone()
    conn.close()
    if result:
        return result
    else:
        return None


if __name__ == '__main__':
    user_id = input("Enter your id: ")
    user_info = get_user_info(user_id)
    if user_info:
        name, email, age = user_info
        print(f"Name: {name}")
        print(f"Email: {email}")
        print(f"Age: {age}")
    else:
        print("User not found.")
